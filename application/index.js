import { fetchModel, deleteFile, readFile, updateLog, appGlobals, linkTooltip } from './editor/js/Globals.js';
import { UIButton, UICheckbox, UINumber, UIHorizontalRule, UIPanel, UIRow, UIText } from './editor/js/libs/ui.js';
import { Strings } from './editor/js/Strings.js';

const globals = appGlobals;
window.globals = appGlobals; // expose to console - by AG
globals.root = document.getElementById('root');
globals.strings = new Strings(globals);
const strings = globals.strings;

// Read machine-model
readFile('/home/volumes/shared/data-model/data-model.json', (response) => {
  // Read log
  globals.machine = JSON.parse(response).machine_model;
  readFile('/home/volumes/log/log.json', (responseText) => {
    if (responseText === 'Error in reading file') {
      // Handle error
    } else {
      globals.log = JSON.parse(responseText);
    }
    buildPage();
  });
});

function buildPage() {
  // Machine model
  const machine = document.getElementById('machine');
  while (machine.firstChild) { machine.removeChild(machine.lastChild); }
  let container = document.createElement("div");
  container.appendChild(document.createElement('h2'));
  container.querySelector('h2').innerHTML = 'Machine model';

  let textWidth = '160px';
  let inputWidth = '60px';

  let panel = new UIPanel().setClass('machine home-box');
  let row = new UIRow()
    .add(new UIText('Machine: ' + globals.machine.model_name));
  panel.add(row);
  row = new UIRow()
    .add(new UIText('Working area: ' +
      globals.machine.working_area.x + 'mm * ' +
      globals.machine.working_area.y + 'mm *  ' +
      globals.machine.working_area.z + 'mm'
    ));
  panel.add(row);
  row = new UIRow()
    .add(new UIHorizontalRule())
    .add(new UIText('The following parameters are retrieved from the defaults of the machine model of the application, but can be customized for tweaking the estimation times'))
    .add(new UIHorizontalRule());
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Laser speed (mm/s): ').setWidth(textWidth))
  .add(new UINumber()
    .setId('laser-speed')
    .setWidth(inputWidth)
    .setPrecision( 0 )
    .setStep(10)
    .setNudge(50)
    .setRange(50, 2000)
    .setValue(globals.machine.laser_speed)
    .onChange((event) => { updateEstimation(); })
  )
  .add(new UIText('Recoating time (s): ').setWidth(textWidth))
  .add(new UINumber()
    .setId('recoating-time')
    .setWidth(inputWidth)
    .setPrecision( 2 )
    .setStep(0.1)
    .setNudge(.1)
    .setRange(0.1, 10)
    .setValue(globals.machine.recoating_time)
    .onChange((event) => { updateEstimation(); })
  )
  panel.add(row);

  row = new UIRow()
    .add(new UIText('Hatching step (mm): ').setWidth(textWidth))
    .add(new UINumber()
      .setId('hatching-distance')
      .setWidth(inputWidth)
      .setPrecision( 3 )
      .setStep(0.01)
      .setNudge(0.005)
      .setRange(0.005, 0.15)
      .setValue(globals.machine.hatching_distance)
      .onChange((event) => { updateEstimation(); })
    )
    .add(new UIText('Material price (€/dm³): ').setWidth(textWidth))
    .add(new UINumber()
      .setId('material-price')
      .setWidth(inputWidth)
      .setPrecision( 2 )
      .setStep(10)
      .setNudge(10)
      .setRange(5, 5000)
      .setValue(globals.machine.material_price)
      .onChange((event) => { updateEstimation(); })
    );

  panel.add(row);

  row = new UIRow()
    .add(new UIText('Support density (0 to 1): ').setWidth(textWidth))
    .add(new UINumber()
      .setId('support-density')
      .setWidth(inputWidth)
      .setPrecision( 2 )
      .setStep(0.05)
      .setNudge(0.05)
      .setRange(0.05, 1)
      .setValue(globals.machine.support_density)
      .onChange((event) => { updateEstimation(); })
    )
    .add(new UIText('Support removal price (€/mm²): ').setWidth(textWidth))
    .add(new UINumber()
    .setId('support-removal')
    .setWidth(inputWidth)
    .setPrecision( 2 )
    .setStep(0.05)
    .setNudge(0.20)
    .setRange(0.01, 100)
    .setValue(globals.machine.support_removal)
    .onChange((event) => { updateEstimation(); })
  );
  panel.add(row);

  row = new UIRow()
    .add(new UIText('Models to floor distance (mm): ').setWidth(textWidth))
    .add(new UINumber()
      .setId('floor-distance')
      .setWidth(inputWidth)
      .setPrecision( 1 )
      .setStep(0.1)
      .setNudge(0.5)
      .setRange(0, 20)
      .setValue(globals.machine.floor_distance)
      .onChange((event) => { updateEstimation(); })
    );
  panel.add(row);

  container.append(panel.dom);
  machine.append(container);

  // Estimation
  const estimation = document.getElementById('estimation');
  while (estimation.firstChild) { estimation.removeChild(estimation.lastChild); }
  container = document.createElement("div");
  container.appendChild(document.createElement('h2'));

  container.querySelector('h2').innerHTML = 'KPIs estimations';
  panel = new UIPanel().setClass('estimation home-box');
  panel
    .add(new UIText('Check many sessions with the same layer thickness to calculate the cumulative production time'));
  row = new UIRow()
    .add(new UIText('Models selected: '))
    .add(new UIText('0').setId('cumulative-count'))
    .add(new UIHorizontalRule());
  panel.add(row);

// ----------------OUTPUTS------------------------

  row = new UIRow()
    .add(new UIText('Total production time: '))
    .add(new UIText().setId('production-time').setValue('0.000'))
    .add(new UIText(' hours'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Sintering time: '))
  .add(new UIText().setId('sintering-time').setValue('0.000'))
  .add(new UIText(' hours'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Recoating time: '))
  .add(new UIText().setId('total-recoating-time').setValue('0.000'))
  .add(new UIText(' hours'));
  panel.add(row);

  // hr-----------------
	panel.add(new UIHorizontalRule());

  row = new UIRow()
    .add(new UIText('Max heigth: '))
    .add(new UIText().setId('max-height').setValue('0'))
    .add(new UIText(' mm'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Number of layers: '))
  .add(new UIText().setId('n-layers').setValue('0'));

  panel.add(row);

  // hr-----------------
	panel.add(new UIHorizontalRule());

  row = new UIRow()
  .add(new UIText('Total volume: '))
  .add(new UIText().setId('total-volume').setValue('0'))
  .add(new UIText(' mm³'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Parts volume: '))
  .add(new UIText().setId('parts-volume').setValue('0'))
  .add(new UIText(' mm³'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Support volume: '))
  .add(new UIText().setId('support-volume').setValue('0'))
  .add(new UIText(' mm³'));
  panel.add(row);

  // hr-----------------
	panel.add(new UIHorizontalRule());

  row = new UIRow()
  .add(new UIText('Support contact area: '))
  .add(new UIText().setId('support-contact').setValue('0'))
  .add(new UIText(' mm²'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Support contact percentage: '))
  .add(new UIText().setId('support-percentage').setValue('0'))
  .add(new UIText(' %'));
  panel.add(row);

  // hr-----------------
	panel.add(new UIHorizontalRule());

  row = new UIRow()
  .add(new UIText('Material cost: '))
  .add(new UIText().setId('material-cost').setValue('0'))
  .add(new UIText(' €'));
  panel.add(row);

  row = new UIRow()
  .add(new UIText('Support removal cost: '))
  .add(new UIText().setId('support-removal-cost').setValue('0'))
  .add(new UIText(' €'));
  panel.add(row);

  // -------------END OUTPUTS--------------------

  row = new UIRow().setId('cumulative-alert')
    .add(new UIHorizontalRule)
    .add(new UIText('Pay attention! You\'ve selected sessions with different layer thickness!'));
  panel.add(row);
  container.append(panel.dom);
  estimation.append(container);

  // Filter

  // List
  let newSession = document.getElementById('new');
  while (newSession.firstChild) { newSession.removeChild(newSession.lastChild); }
  let anchor = document.createElement('a');
  anchor.setAttribute('href', '/editor/');
  let button = new UIButton('Start a New session')    
    .onClick(() => { window.location = ('/editor/'); });      
  linkTooltip(button.dom, strings.getKey('tooltip/home/empty'));
  anchor.appendChild(button.dom);
  newSession.appendChild(anchor);

  if (globals.log !== undefined) {
    parseModels(globals.log.models);
  } else {
    console.log("Log parameter not set");
  }
}

function parseModels(models) {
  let container = document.createElement("div");
  models.forEach(model => {
    let panel = new UIPanel().setClass('model home-box');
    let name = new UIText(model.name).setClass('model__name');    
    let button = new UIButton('New session')
      .setClass('new__button')
      .onClick(() => {
        linkToSession(model.name);    
    });
    linkTooltip(button.dom, strings.getKey('tooltip/home/new'));
    panel.add(name);
    panel.add(button);

    let deleteButton = new UIButton('X')
      .setClass('delete-model')
      .setWidth('50px')
      .onClick(() => {
        removeModel(deleteButton.modelObject);
        updateLog();
        buildPage();
      });
    deleteButton.modelObject = model;
    linkTooltip(deleteButton.dom, strings.getKey('tooltip/home/deletemodel'));
    panel.add(deleteButton);

    parseOperations(model, panel);
    container.append(panel.dom);
  });
  const list = document.querySelector('#list .wrapper');
  while (list.firstChild) { list.removeChild(list.lastChild); }
  list.appendChild(container);
};

function parseOperations(model, panel) {
  globals.log.parameters
    .filter(value => { return value.model === model.name;})
    .forEach((value, index) => {
      readFile('/home/volumes/upload/parameters/' + value.name, (response) => {
        let thickness = JSON.parse(response).simulation.layer_thickness;
        let parametersRow = new UIRow().setClass('parameters');
        let rawTime = value.name.split('.')[0];
        let rawDate = rawTime.split('T')[0];
        rawDate = rawDate.slice(0, 2) + '/' + rawDate.slice(2, 4) + '/' + rawDate.slice(4, 6);
        let rawHour = rawTime.split('T')[1];
        rawHour = rawHour.slice(0, 2) + ':' + rawHour.slice(2, 4) + ':' + rawHour.slice(4, 6);
        let date  = rawDate + ', ' + rawHour;
        parametersRow
          .add(new UICheckbox(false).setClass('estimation-check').setId(value.name).onChange((event) => {
            event.srcElement.setAttribute('thickness', thickness);
            updateEstimation();
          }))
          .add(new UIText(date).setClass('parameters__name'))
          .add(new UIText('Layer thickness: ' + thickness));
        let button = new UIButton('Open')
            .setClass('parameters__button')
            .onClick(() => {
              linkToSession(model.name, value.name);
            });
        linkTooltip(button.dom, strings.getKey('tooltip/home/open')); 
        parametersRow.add(button);
        
        parseResults(value, parametersRow);

        let deleteButton = new UIButton('X').setClass('delete-parameters').setWidth('50px').onClick(() => {
          removeSession(deleteButton.parametersObject);
          updateLog();
          buildPage();
        });
        deleteButton.parametersObject = value;
        linkTooltip(deleteButton.dom, strings.getKey('tooltip/home/deletesession'));
        parametersRow.add(deleteButton);
        panel.add(parametersRow);
      });
  });
}

function parseResults(parameters, row) {
  let orient = globals.log.operations.orients
    .find( value => { return value.parameters === parameters.name;});
  if (orient !== undefined) {
    let orientedModel = orient.model.split('.')[0] + '_oriented_' + orient.parameters.split('.')[0] + '.stl';
    let button = new UIButton('Oriented Model').onClick(() => { linkToModel('/home/volumes/upload/models/oriented/' + orientedModel) });
    linkTooltip(button.dom, strings.getKey('tooltip/home/download'));
    row.add(button);
    button = new UIButton('Orientation File').onClick(() => { linkToFile('/home/volumes/orient-output/' + orient.name) });
    linkTooltip(button.dom, strings.getKey('tooltip/home/download'));
    row.add(button);

    let simulation = globals.log.operations.simulations
      .find( value => { return value.parameters === parameters.name;});
    if (simulation !== undefined) {
      button = new UIButton('Simulation File').onClick(() => {linkToFile('/home/volumes/simulation-output/' + simulation.name)});
      linkTooltip(button.dom, strings.getKey('tooltip/home/download'));
      row.add(button);      

      readFile('/home/volumes/simulation-output/' + simulation.name, (response) => {
        let values = JSON.parse(response);
        row.dom.querySelector('input').setAttribute('human_labor', values.human_labor);
        row.dom.querySelector('input').setAttribute('machining_time', values.machining_time);
        row.dom.querySelector('input').setAttribute('material_usage', values.material_usage);
        row.dom.querySelector('input').setAttribute('model_height', values.model_height);
        row.dom.querySelector('input').setAttribute('n_slices', values.n_slices);
        row.dom.querySelector('input').setAttribute('part_surface', values.part_surface);
        row.dom.querySelector('input').setAttribute('part_volume', values.part_volume);
        row.dom.querySelector('input').setAttribute('support_platform_area', values.support_platform_area);
        row.dom.querySelector('input').setAttribute('surface_quality', values.surface_quality);
      });
    }
  } else {
    // This is a very weak solution ... - by AG
    row
      .add(new UIButton('... orientation in progress - click to update ...')
      .onClick(() => {
        window.location.reload();
      })
    );
  }
}

function linkToSession(model, parameters) {
  let queryString = '?model=' + model;
  if (parameters !== undefined) { queryString += '&parameters=' + parameters; }
  window.location = ('/editor/' + queryString);
}

function linkToFile(path) {
  readFile(path, (responseText) => {
    var blob = new Blob([responseText], {type: "application/json"});
    var url = window.URL.createObjectURL(blob);
    var anchor = document.createElement("a");
    anchor.href = url;
    anchor.download = path.split(/\/home\/volumes\/*/g)[1];
    anchor.click();
    window.URL.revokeObjectURL(url);
    document.removeChild(anchor);
  })
}

function linkToModel(path) {
	fetchModel(path, (response) => {
		var anchor = document.createElement("a");
		anchor.href = '/' + response;
		anchor.download = path.split(/.*\//g,)[1];
		anchor.click();
		document.removeChild(anchor);
	});
}

function removeSession(object) {
  if (globals.log !== undefined) {
    globals.log.parameters = globals.log.parameters.filter((element) => {
      if (element.name === object.name) { deleteFile('/home/volumes/upload/parameters/' + element.name); }
      return element.name !== object.name;
    } );
    globals.log.operations.orients = globals.log.operations.orients.filter((element) => {
      if (element.parameters === object.name) { deleteFile('/home/volumes/orient-output/' + element.name); }
      return element.parameters !== object.name;
    });
    globals.log.operations.simulations = globals.log.operations.simulations.filter((element) => {
      if (element.parameters === object.name) { deleteFile('/home/volumes/simulation-output/' + element.name); }
      return element.parameters !== object.name;
    });
    let orientedUri = object.model.split('.')[0] + '_oriented_' + object.name.split('.')[0] + '.stl';
    deleteFile('/home/volumes/upload/models/oriented/' + orientedUri);
  } else {
    // returns undefined if log is not set
    return undefined;
  }
}

function removeModel(object) {
  if (globals.log !== undefined) {
    globals.log.models = globals.log.models.filter(element => element.name !== object.name );
    globals.log.parameters.forEach((value) => {
      if (value.model === object.name) { removeSession(value); }
    });
    deleteFile('/home/volumes/upload/models/' + object.name);
  } else {
    // returns undefined if log is not set
    return undefined;
  }
}

function updateEstimation() {
  // Parameters from machine model
  let laser_speed = parseFloat(document.getElementById('laser-speed').value);
  let hatching_distance = parseFloat(document.getElementById('hatching-distance').value);
  let recoating_time  = parseFloat(document.getElementById('recoating-time').value);
  //let preload_speed  = parseFloat(document.getElementById('preload-sp.valueeed '));
  //let return_speed  = parseFloat(document.getElementById('return-sp.valueeed '));
  let support_density  = parseFloat(document.getElementById('support-density').value);
  let floor_distance = parseFloat(document.getElementById('floor-distance').value);
  let material_price  = parseFloat(document.getElementById('material-price').value);
  let support_removal = parseFloat(document.getElementById('support-removal').value);

  // Cumulative parameters from sessions
  //let cumulative_machining = 0;
  //let cumulative_material = 0;
  let max_height;
  let max_layer;
  let total_volume;
  let total_area;
  let cumulative_volume;
  let cumulative_support_volume;
  let cumulative_support_platform_area;
  let cumulative_support_contact;
  let support_relative;


  let sintering_time = 0;
  let sintering_speed_layer = (((1/laser_speed)/hatching_distance));
  let cumulative_recoating_time = 0;
  let current_thickness = 0;
  let production_time = 0;

  let material_cost = 0;
  let support_rem_cost = 0;

  let checkedSessions = [];
  document.querySelectorAll('.estimation-check').forEach((element) => {
    if (element.checked) { checkedSessions.push(element); }
  });
  document.getElementById('cumulative-count').innerHTML = checkedSessions.length;
  if (checkedSessions.length) {
    let tmp = checkedSessions[0].getAttribute('thickness');
    if (checkedSessions.every(element => element.getAttribute('thickness') === tmp)) {
      document.getElementById('cumulative-alert').style.setProperty('display', 'none');

      max_height = checkedSessions.reduce((previous, current, index) => {
        let tmp = parseFloat(current.getAttribute('model_height'));
        return (tmp > previous) ? tmp : previous;
      }, 0);

      max_layer = checkedSessions.reduce((previous, current, index) => {
        let tmp = parseFloat(current.getAttribute('n_slices'));
        return (tmp > previous) ? tmp : previous;
      }, 0);

      cumulative_volume = checkedSessions.reduce((previous, current, index) => {
        return previous + parseFloat(current.getAttribute('part_volume'));
      }, 0);

      cumulative_support_volume = checkedSessions.reduce((previous, current, index) => {
        return previous + parseFloat(current.getAttribute('material_usage'));
      }, 0);

      cumulative_support_platform_area = checkedSessions.reduce((previous, current, index) => {
        return previous + parseFloat(current.getAttribute('support_platform_area'));
      }, 0);

      cumulative_support_contact = checkedSessions.reduce((previous, current, index) => {
        return previous + parseFloat(current.getAttribute('surface_quality'));
      }, 0);

      total_area = checkedSessions.reduce((previous, current, index) => {
        return previous + parseFloat(current.getAttribute('part_surface'));
      }, 0);


      // If necessary do operations cumulatives in here
      current_thickness = parseFloat(tmp);

      max_height = max_height + floor_distance;
      max_layer = Math.round(max_layer+(floor_distance/current_thickness));

      cumulative_support_volume = (cumulative_support_volume + (cumulative_support_platform_area * floor_distance)*support_density);
      total_volume = cumulative_volume + cumulative_support_volume;

      support_relative = cumulative_support_contact / total_area * 100;

      sintering_time = (cumulative_volume+cumulative_support_volume)*(sintering_speed_layer/current_thickness)/3600;
      cumulative_recoating_time = (recoating_time * max_layer)/3600;
      production_time = sintering_time + cumulative_recoating_time;

      material_cost = total_volume * material_price / 1000000;
      support_rem_cost = cumulative_support_contact * support_removal;

      // Assignment to node elements of accumulations
      document.getElementById('max-height').innerHTML = max_height.toFixed(2);
      document.getElementById('sintering-time').innerHTML = sintering_time.toFixed(3);
      document.getElementById('total-recoating-time').innerHTML = cumulative_recoating_time.toFixed(3);
      document.getElementById('production-time').innerHTML = production_time.toFixed(3);
      document.getElementById('n-layers').innerHTML = max_layer.toFixed(0);
      document.getElementById('parts-volume').innerHTML = cumulative_volume.toFixed(0);
      document.getElementById('support-volume').innerHTML = cumulative_support_volume.toFixed(0);
      document.getElementById('total-volume').innerHTML = total_volume.toFixed(0);
      document.getElementById('support-contact').innerHTML = cumulative_support_contact.toFixed(0);
      document.getElementById('support-percentage').innerHTML = support_relative.toFixed(0);
      document.getElementById('material-cost').innerHTML = material_cost.toFixed(2);
      document.getElementById('support-removal-cost').innerHTML = support_rem_cost.toFixed(2);
    } else {
      document.getElementById('cumulative-alert').style.setProperty('display', 'block');
    }
  } else {
    document.getElementById('max-height').innerHTML = '0';
    document.getElementById('sintering-time').innerHTML = '0';
    document.getElementById('total-recoating-time').innerHTML = '0';
    document.getElementById('production-time').innerHTML = '0';
    document.getElementById('n-layers').innerHTML = '0';
    document.getElementById('parts-volume').innerHTML = '0';
    document.getElementById('support-volume').innerHTML = '0';
    document.getElementById('total-volume').innerHTML = '0';
    document.getElementById('support-contact').innerHTML = '0';
    document.getElementById('support-percentage').innerHTML = '0';
    document.getElementById('material-cost').innerHTML = '0';
    document.getElementById('support-removal-cost').innerHTML = '0';
  }
}