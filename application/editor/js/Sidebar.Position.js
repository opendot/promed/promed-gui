import * as THREE from 'three';
import { pushToGround } from './Globals.js';

import { UIPanel, UIRow, UIText, UINumber } from './libs/ui.js';

function SidebarPosition( editor ) {
  const strings = editor.strings;

	const signals = editor.signals;

	const container = new UIPanel();
	container.setBorderTop( '0' );
	container.setPaddingTop( '20px' );
	container.setDisplay( 'block' );

	// position

	const objectPositionRow = new UIRow();
	const objectPositionX = new UINumber().setPrecision( 2 ).setWidth( '50px' ).onChange( update ).setId('positionX').setValue(0);
	const objectPositionY = new UINumber().setPrecision( 2 ).setWidth( '50px' ).onChange( update ).setId('positionY').setValue(0);
	const objectPositionZ = new UINumber().setPrecision( 2 ).setWidth( '50px' ).onChange( update ).setId('positionZ').setValue(0);

	objectPositionRow.add( new UIText( strings.getKey( 'sidebar/object/position' ) ).setWidth( '90px' ) );
	objectPositionRow.add( objectPositionX, objectPositionY, objectPositionZ );

	container.add( objectPositionRow );

	// rotation

	const objectRotationRow = new UIRow();
	const objectRotationX = new UINumber().setStep( 10 ).setNudge( 0.1 ).setWidth( '50px' ).onChange( update ).setId('rotationX').setValue(0);
	const objectRotationY = new UINumber().setStep( 10 ).setNudge( 0.1 ).setWidth( '50px' ).onChange( update ).setId('rotationY').setValue(0);
	const objectRotationZ = new UINumber().setStep( 10 ).setNudge( 0.1 ).setWidth( '50px' ).onChange( update ).setId('rotationZ').setValue(0);

	objectRotationRow.add( new UIText( strings.getKey( 'sidebar/object/rotation' ) ).setWidth( '90px' ) );
	objectRotationRow.add( objectRotationX, objectRotationY, objectRotationZ );

	container.add( objectRotationRow );

	//

	function update() {	
		// All the scene updates in here - by AG
		const model = editor.scene.children[editor.config.getKey('scene/modelRef')];
		model.position.set(
			parseFloat(document.getElementById('positionX').value),
			parseFloat(document.getElementById('positionY').value),
			parseFloat(document.getElementById('positionZ').value)
		);		

		model.rotation.set(
			parseFloat(document.getElementById('rotationX').value),
			parseFloat(document.getElementById('rotationY').value), 
			parseFloat(document.getElementById('rotationZ').value)
		);
		
		pushToGround(model);

		signals.rendererUpdated.dispatch();		
	}

	function updateRows() {

		const properties = {
			'rotation' : objectRotationRow,
			'position' : objectPositionRow
		};

		for ( const property in properties ) {

			const uiElement = properties[ property ];
			uiElement.setDisplay( 'block' );
			uiElement.setId(property);

			if ( Array.isArray( uiElement ) === true ) {

				for ( let i = 0; i < uiElement.length; i ++ ) {

					uiElement[ i ].setDisplay( 'block' );

				}

			}
		}
	}	

	updateRows();

  return container;
}

export { SidebarPosition };