import { UITabbedPanel } from './libs/ui.js';

import { SidebarOrient } from './Sidebar.Orient.js';
import { SidebarProcess } from './Sidebar.Process.js';
import { SidebarPosition } from './Sidebar.Position.js';

// import { SidebarObject } from './Sidebar.Object.js';
// import { SidebarGeometry } from './Sidebar.Geometry.js';
// import { SidebarMaterial } from './Sidebar.Material.js';

function SidebarProperties( editor ) {

	const strings = editor.strings;

	const container = new UITabbedPanel();
	container.setId( 'properties' );

	container.addTab( 'orient', strings.getKey( 'sidebar/properties/orient' ), new SidebarOrient( editor ) );
	container.addTab( 'process', strings.getKey( 'sidebar/properties/process' ), new SidebarProcess( editor ) );
	container.addTab( 'position', strings.getKey( 'sidebar/properties/position' ), new SidebarPosition( editor ) );

	container.select( 'orient' );

	return container;

}

export { SidebarProperties };
