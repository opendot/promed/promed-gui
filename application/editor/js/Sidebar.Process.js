import * as THREE from 'three';

import { UIPanel, UIRow, UIText, UINumber } from './libs/ui.js';

function SidebarProcess( editor ) {
  const strings = editor.strings;

	const signals = editor.signals;

	const container = new UIPanel();
	container.setBorderTop( '0' );
	container.setPaddingTop( '20px' );
	container.setDisplay( 'block' );

	// LaserSpeed
	
	const processLaserSpeedRow = new UIRow();
	const processLaserSpeed = new UINumber().setPrecision( 0 ).setStep(10).setNudge(50).setWidth( '100px' ).setRange(100, 2000).onChange( update ).setValue(800);
		
	processLaserSpeedRow.add( new UIText( strings.getKey( 'sidebar/process/laserspeed' ) ).setWidth( '200px' ) );
	processLaserSpeedRow.add( processLaserSpeed );
	
	container.add( processLaserSpeedRow );

	// Hatching step
	
	const processHatchingStepRow = new UIRow();
	const processHatchingStep = new UINumber().setPrecision( 3 ).setStep(0.01).setNudge(0.005).setWidth( '100px' ).setRange(0, 0.15).onChange( update ).setValue(0.080);
		
	processHatchingStepRow.add( new UIText( strings.getKey( 'sidebar/process/hatchingstep' ) ).setWidth( '200px' ) );
	processHatchingStepRow.add( processHatchingStep );
	
	container.add( processHatchingStepRow );


	// Layer height

	const processLayerRow = new UIRow();
	const processLayer = new UINumber().setPrecision( 3 ).setStep(0.01).setNudge(0.005).setWidth( '100px' ).setRange(0.01, 0.07).onChange( update ).setValue(0.02);
	
	processLayerRow.add( new UIText( strings.getKey( 'sidebar/process/layer' ) ).setWidth( '200px' ) );
	processLayerRow.add( processLayer );

	container.add( processLayerRow );

	// Support Angle

	const processAngleRow = new UIRow();
	const processAngle = new UINumber().setPrecision( 0 ).setStep(6).setNudge(5).setWidth( '100px' ).setRange(0, 90).onChange( update ).setValue(30);
	
	processAngleRow.add( new UIText( strings.getKey( 'sidebar/process/angle' ) ).setWidth( '200px' ) );
	processAngleRow.add( processAngle );

	container.add( processAngleRow );

	// Support Density
	
	const processDensityRow = new UIRow();
	const processDensity = new UINumber().setPrecision( 2 ).setStep(0.1).setNudge(0.05).setWidth( '100px' ).setRange(0, 1).onChange( update ).setValue(0.5);
	
	processDensityRow.add( new UIText( strings.getKey( 'sidebar/process/density' ) ).setWidth( '200px' ) );
	processDensityRow.add( processDensity );

	container.add( processDensityRow );

	// Support Removal effort
	
	const processEffortRow = new UIRow();
	const processEffort = new UINumber().setPrecision( 2 ).setStep(0.1).setNudge(0.05).setWidth( '100px' ).setRange(0, 1).onChange( update ).setValue(0.5);
	
	processEffortRow.add( new UIText( strings.getKey( 'sidebar/process/effort' ) ).setWidth( '200px' ) );
	processEffortRow.add( processEffort );

	container.add( processEffortRow );



	//








	function update() {
		const object = editor.selected;

		// All the scene updates in here

	}

	function updateRows() {

		const properties = {
			'layer'        : processLayerRow,
			'angle'        : processAngleRow,
			'density'      : processDensityRow,		
			'laserspeed'   : processLaserSpeedRow,
			'hatchingstep' : processHatchingStepRow,
			'effort'       : processEffortRow,
			
		};
	
		for ( const property in properties ) {
	
			const uiElement = properties[ property ];
			uiElement.setDisplay( 'block' );
			uiElement.setId(property);
	
			if ( Array.isArray( uiElement ) === true ) {
	
				for ( let i = 0; i < uiElement.length; i ++ ) {
	
					uiElement[ i ].setDisplay( 'block' );
	
				}

			} 
		}
	}
		
	updateRows();					
	
  return container;
}

export { SidebarProcess };