import { UIRow, UIText, UIPanel, UIDiv } from './libs/ui.js';

function SidebarResults() {

  const strings = editor.strings;
  
  const container = new UIPanel();
  container.setId('results');

  container.setBorderTop( '0' );
  container.setStyle('padding-top', '0px');
	container.setDisplay( 'none' );  

  // Div for loading message

  const loading = new UIDiv().setId('results__loading');
  loading.add(new UIText(strings.getKey('sidebar/results/message1')));
  loading.add(new UIText(strings.getKey('sidebar/results/message2')));
  loading.setDisplay( 'none' );
  
  container.add(loading);

  // Div for all the results

  const results = new UIDiv().setId('results__values');

  // process

  const processRow = new UIRow();
  processRow.setId('results-title');

  const processText = new UIText(strings.getKey('sidebar/results/process'));
  processRow.add(processText);
  results.add(processRow);

  // height

  const heightRow = new UIRow(); 
  const heightText = new UIText(strings.getKey('sidebar/results/height')).setWidth( '200px' );
  heightRow.add(heightText);
  const heightResult = new UIText('0.00').setId('result-height');
  heightRow.add(heightResult);
  results.add(heightRow);

  // shadow

  const shadowRow = new UIRow(); 
  const shadowText = new UIText(strings.getKey('sidebar/results/shadow')).setWidth( '200px' );
  shadowRow.add(shadowText);
  const shadowResult = new UIText('0.00').setId('result-shadow');
  shadowRow.add(shadowResult);
  results.add(shadowRow );

  // volume

  const volumeRow = new UIRow(); 
  const volumeText = new UIText(strings.getKey('sidebar/results/volume')).setWidth( '200px' );
  volumeRow.add(volumeText);
  const volumeResult = new UIText('0.00').setId('result-volume');
  volumeRow.add(volumeResult);
  results.add(volumeRow);

  // support

  const supportRow = new UIRow(); 
  const supportText = new UIText(strings.getKey('sidebar/results/support')).setWidth( '200px' );
  supportRow.add(supportText);
  const supportResult = new UIText('0.00').setId('result-support');
  supportRow.add(supportResult);
  results.add(supportRow);

  // sintered

  const sinteredRow = new UIRow(); 
  const sinteredText = new UIText(strings.getKey('sidebar/results/sintered')).setWidth( '200px' );
  sinteredRow.add(sinteredText);
  const sinteredResult = new UIText('0.00').setId('result-sintered');
  sinteredRow.add(sinteredResult);
  results.add(sinteredRow);

  //
  results.setDisplay( 'none' )
  container.add(results);
  return container;
}

export { SidebarResults };