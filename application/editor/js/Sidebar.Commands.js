// App commands - by AG

import { UIButton, UIRow, UIPanel, UINumber } from './libs/ui.js';
import { fetchModel, updateFromOrient, updateFromSimulation, readFile } from './Globals.js';

function SidebarCommands(editor) {

  const strings = editor.strings;
	const signals = editor.signals;

  const container = new UIPanel();
  container.setId('commands');

  container.setBorderTop( '1' );
  container.setDisplay('block');

  const commandsButtonRow = new UIRow();

  const buttonDatabase = new UIButton(strings.getKey('sidebar/commands/database'));
  buttonDatabase.onClick(() => {
    // delete temporary files
    var clean_request = new XMLHttpRequest();
    clean_request.open('POST', '../../scripts/clean_temporaries.php');
    clean_request.onreadystatechange = function() {
      if (clean_request.readyState == 4 && clean_request.status == 200) {
        // action to get back to app home
        window.location.replace("/")
      }
    }
    clean_request.send();    
  }); 
  commandsButtonRow.add(buttonDatabase);

  const buttonOrient = new UIButton(strings.getKey('sidebar/commands/orient'))
    .addClass('Button--disabled')
    .setId('orient-button')
    .onClick(() => {
    if (editor.config.getKey('scene/modelRef')) {
      document.getElementById('results').style.setProperty('display', 'block');
      document.getElementById('results__loading').style.setProperty('display', 'block');
      document.getElementById('results__values').style.setProperty('display', 'none');
      // To parse texts into arrays (insensitive to commas or not between nested arrays)
      let forb = document.querySelector('#forbidden textarea').value;
      if (forb !== '[]') {
        forb = document.querySelector('#forbidden textarea').value.split(/\].{0,1}\[/);
        forb.forEach((element, index, array) => {
          array[index] = element.replace(/\[|\]/g, '');
          array[index] = array[index].split(',');
        });
       forb = forb[0].reduce((previous, current, index) => {
         if (index % 3 === 0) { previous.push([]); }
         previous[previous.length - 1].push(parseFloat(current));
         return previous;
       }, []);
      } else {
        forb = [];
      };

      let inputFields = {
        'directions': document.querySelector('#directions input').value,
        'angle': document.querySelector('#angle input').value,
        'buffer': document.querySelector('#buffer input').value,
        'height': document.querySelector('#height input').value,
        'shadow': document.querySelector('#shadow input').value,
        'contact': document.querySelector('#contact input').value,
        'volume': document.querySelector('#volume input').value,
        'critical': document.querySelector('#critical input').value,
        'cone': document.querySelector('#cone input').value,
        'layer': document.querySelector('#layer input').value,
        'density': document.querySelector('#density input').value,
        'forbidden': document.querySelector('#forbidden textarea').value
      };

      // Variables for the orientation processing
      let n_dirs = parseFloat(document.querySelector('#directions input').value);
      let overhang_ang = parseFloat(document.querySelector('#angle input').value);
      let buffer_size = parseFloat(document.querySelector('#buffer input').value);
      let w_height = parseFloat(document.querySelector('#height input').value);
      let w_shadow_area = parseFloat(document.querySelector('#shadow input').value);
      let w_support_contact = parseFloat(document.querySelector('#contact input').value);
      let w_support_volume = parseFloat(document.querySelector('#volume input').value);
      let crit_srf_boost = parseFloat(document.querySelector('#critical input').value);
      let forb_cone_angle = parseFloat(document.querySelector('#cone input').value);

      // Variables for the simulation processing
      let sim_overhang_ang = parseFloat(document.querySelector('#angle input').value);
      let layer_thickness = parseFloat(document.querySelector('#layer input').value);

      let dataModel = editor.config.getKey('data-model/parameters');

      let recoating_time = dataModel.recoating_time;      

      let sintering_time = 
        (1 / parseFloat(document.querySelector('#laserspeed input').value)) / 
        parseFloat(document.querySelector('#hatchingstep input').value) /
        layer_thickness;
      let supp_density = parseFloat(document.querySelector('#density input').value);

      // Conversion formulas Tiziano
      //let a = b > a ? a : b

      // Unitizing weights sums
      let w_total = w_height + w_shadow_area + w_support_contact + w_support_volume;
      if (w_total==0) {
        w_height = 0.25;
        w_shadow_area = 0.25;
        w_support_contact = 0.25;
        w_support_volume = 0.25;
      } else {
        w_height = w_height/w_total;
        w_shadow_area = w_shadow_area/w_total;
        w_support_contact = w_support_contact/w_total;
        w_support_volume = w_support_volume/w_total;
      }

      // Making srf boost similar to weights
      crit_srf_boost = crit_srf_boost/100;

      // JSON generation
      let orientTmp = {
        "n_dirs"             : n_dirs,
        "overhang_ang"       : overhang_ang,
        "buffer_size"        : buffer_size,
        "w_height"           : w_height,
        "w_shadow_area"      : w_shadow_area,
        "w_support_contact"  : w_support_contact,
        "w_support_volume"   : w_support_volume,
        "crit_srf_boost"     : crit_srf_boost,
        "forb_cone_angle"    : forb_cone_angle,
        "forb_dirs"          : forb
      };

      let simulationTmp = {
        "overhang_ang"     : sim_overhang_ang,
        "layer_thickness"  : layer_thickness,
        "recoating_time"   : recoating_time,
        "sintering_time"   : sintering_time,        
        "supp_density"     : supp_density
      }

      // console.log("Orient settings");
      // console.log(orientTmp);
      // console.log("Simulation settings");
      // console.log(simulationTmp);

      editor.config.setKey(
        'process/orient', orientTmp,
        'process/simulation', simulationTmp,
        'process/inputFields', inputFields
      );

      let processForm = new FormData();
      processForm.append('model', editor.config.getKey('process/model'));
      processForm.append('modelName', editor.config.getKey('process/modelName'));
      processForm.append('orient', JSON.stringify(editor.config.getKey('process/orient')));
      processForm.append('simulation', JSON.stringify(editor.config.getKey('process/simulation')));
      processForm.append('existingModel', editor.config.getKey('process/existingModel'));
      processForm.append('inputFields', JSON.stringify(editor.config.getKey('process/inputFields')));

      var clean_request = new XMLHttpRequest();
	    clean_request.open('POST', '../../scripts/upload.php');
	    clean_request.onreadystatechange = function() {
	      if (clean_request.readyState == 4 && clean_request.status == 200) {
	      	var response = clean_request.response;
          if (response !== 'ERR') {
            // Update results tab
            let resultsPaths = JSON.parse(response);

            // Retrieve orient results
            readFile(resultsPaths.orient_destination, (response) => {
              let orientResults = JSON.parse(response);
              updateFromOrient(orientResults);
              signals.rendererUpdated.dispatch();

              // upload oriented model
              var upload_request = new XMLHttpRequest();
	            upload_request.open('POST', '../../scripts/upload_oriented.php');
	            upload_request.onreadystatechange = function() {
                console.log(upload_request.response);
                editor.config.setKey('process/orientedModel', upload_request.response);
              }
              let orientedName = resultsPaths.parameters_destination.split('/');
              orientedName =
                editor.config.getKey('process/modelName').split('.')[0]
                + '_oriented_'
                + orientedName[orientedName.length - 1].split('.')[0]
                + '.stl';
              let uploadForm = new FormData();
              uploadForm.append('model', editor.config.getKey('process/model'))
              uploadForm.append('filename', orientedName);
              upload_request.send(uploadForm);
            });

            // Retrieve simulation results
            readFile(resultsPaths.simulation_destination, (response) => {
              let simulationResults = JSON.parse(response);
              updateFromSimulation(simulationResults);
            });
          }
        } else {
          if (clean_request.status !== 200) {
            // TO DO - handle errors in here - by AG
            console.log('ERROR: ' + clean_request.status);
          }
	      }
        // end loadwheel here
      }
      clean_request.send(processForm);
    } else {
      console.log('Model not set');
    }
  });

  commandsButtonRow.add(buttonOrient);

  const buttonSimulate = new UIButton(strings.getKey('sidebar/commands/simulate'))
    .addClass('Button--disabled')
    .setId('ml-button')
    .onClick(() => {
      if (editor.config.getKey('process/completed')) {
        //
      } else {
        console.log('Not available yet')
      }
    });
  commandsButtonRow.add(buttonSimulate);

  const buttonSave = new UIButton(strings.getKey('sidebar/commands/save'))
    .addClass('Button--disabled')
    .setId('save-button')
    .onClick(() => {
      if (editor.config.getKey('process/completed')) {
        let filename = editor.config.getKey('process/orientedModel');
		    fetchModel(filename, (response) => {
			    var anchor = document.createElement("a");
			    anchor.href = '/' + response;
			    anchor.download = filename;
			    anchor.click();
			    document.removeChild(anchor);
		    });
      } else {
        console.log('Not available yet')
      }
    });
  commandsButtonRow.add(buttonSave);

  container.add(commandsButtonRow);

  return container;
}

export { SidebarCommands };