// Global variables and functions for managing the app - by AG

import { MathUtils } from "../../build/three.module.js";

const appGlobals = new Object();

function easyDate() {
  let t = new Date();
  let s =
    t.getFullYear().toString() +
    (t.getMonth() + 1).toString().padStart(2, '0') +
    t.getDate().toString().padStart(2, '0') +
    'T' +
    t.getHours().toString().padStart(2, '0') +
    t.getMinutes().toString().padStart(2, '0') +
    t.getSeconds().toString().padStart(2, '0');
  return s;
}

function readFile(url, callback){
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    callback(xhr.responseText);
  }
  let fileForm = new FormData();
  fileForm.append('url', url);
  xhr.open("POST", '../../scripts/read_file.php');
  xhr.send(fileForm);
}

function checkFile(url, callback){
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    callback(xhr.responseText);
  }
  let fileForm = new FormData();
  fileForm.append('url', url);
  xhr.open("POST", '../../scripts/check_file.php');
  xhr.send(fileForm);
}

function updateLog() {
  if (!appGlobals.log != undefined) {
    var xhr = new XMLHttpRequest();
    let fileForm = new FormData();
    fileForm.append('data', JSON.stringify(appGlobals.log));
    xhr.open("POST", '../../scripts/update_log.php');
    xhr.onload = function() { return xhr.response; }
    xhr.send(fileForm);
  } else {
    return "Log variable not set and cannot be updated";
  }
}

function fetchModel(url, callback){
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    callback(xhr.response);
  }
  let fileForm = new FormData();
  fileForm.append('url', url);
  xhr.open("POST", '../../scripts/read_file.php');
  xhr.send(fileForm);
}

function pushToGround(mesh) {
  mesh.geometry.computeBoundingBox();
  mesh.geometry.center();
  mesh.geometry.computeBoundingSphere();
  mesh.position.set(
    -mesh.geometry.boundingSphere.center.x,
    -mesh.geometry.boundingSphere.center.y,
    -mesh.geometry.boundingSphere.center.z
  );
  let box = new THREE.Box3();
  box.setFromObject(mesh);
  mesh.position.z += box.max.z;
}

function deleteFile(url) {
  var xhr = new XMLHttpRequest();
  let fileForm = new FormData();
  fileForm.append('url', url);
  xhr.open("POST", '../../scripts/delete_file.php');
  xhr.onload = function() { return xhr.response; }
  xhr.send(fileForm);
}

function updateFromOrient(orientResults) {
  // Set results into panel
  document.getElementById('results__loading').style.setProperty('display', 'none');
  const results = document.getElementById('results__values');
  results.style.setProperty('display', 'block');
  results.querySelector('#result-height').innerHTML = orientResults.opt_height.toFixed(2) + ' mm²';
  results.querySelector('#result-shadow').innerHTML = orientResults.opt_shadow_area.toFixed(2) + ' mm²';
  results.querySelector('#result-volume').innerHTML = 0.0.toFixed(2) + ' mm³';
  results.querySelector('#result-support').innerHTML = orientResults.opt_supp_volume.toFixed(2) + ' mm³';
  results.querySelector('#result-sintered').innerHTML = orientResults.opt_contact_area.toFixed(2) + ' mm²';
  
  // Update model rotation
  // Updates model
  const model = editor.scene.children[editor.config.getKey('scene/modelRef')];
  let optimalBuildDirection = new THREE.Vector3(
    orientResults.opt_build_dir[0],
    orientResults.opt_build_dir[1],
    orientResults.opt_build_dir[2]
  ); 
  updateRotation(model, optimalBuildDirection);
  pushToGround(model);
  editor.config.setKey('process/completed', true);
  document.querySelector('#ml-button').classList.remove('Button--disabled');
  document.querySelector('#save-button').classList.remove('Button--disabled');  

  // Updates input fields
  document.getElementById('rotationX').value = parseFloat(model.rotation._x.toFixed(2));
  document.getElementById('rotationY').value = parseFloat(model.rotation._y.toFixed(2));
  document.getElementById('rotationZ').value = parseFloat(model.rotation._z.toFixed(2));
}

function updateFromSimulation(simulationResults) {
  //console.log(simulationResults);
}

function updateRotation(model, vector) {
  
  let optDir = new THREE.Vector3(vector.x,vector.y,vector.z); 
  let vertDir = new THREE.Vector3(0,1,0);
  optDir=optDir.normalize();

  console.log("normalized vector optDir");
  console.log(optDir);

  if (optDir.x == 0 && optDir.y == 0 && optDir.z == -1) {

    model.rotation.set(0, 0, 0);
    model.rotateX(Math.PI);

    console.log("Vertical flip around x");

  } else if (optDir.x == 0 && optDir.y == 0 && optDir.z == 1) {

    model.rotation.set(0, 0, 0);
    console.log("Reset rotation to world coordinate");

  } else {

    model.rotation.set(0, 0, 0);
    let rotationAxis = new THREE.Vector3();        //Working alternative
    rotationAxis.crossVectors(optDir,vertDir);     //Working alternative
    rotationAxis = rotationAxis.normalize();
    let rotationAngle = optDir.angleTo(vertDir);
    //let rotationAngle = Math.acos(dotProduct);
    //let q = new THREE.Quaternion();
    //q.setFromAxisAngle(rotationAxis, rotationAngle);
    model.rotateOnAxis(rotationAxis,rotationAngle);

    console.log("Standard Rotation");
    console.log("---");
    console.log("rotationAngle new");
    console.log(rotationAngle);
    console.log("rotationAngle deg");
    console.log((rotationAngle*180/Math.PI));
    console.log("rotationAxis");
    console.log(rotationAxis);
    console.log("---");
  }
}

function linkTooltip(element, text) {
  let tooltip = document.createElement('div');
  tooltip.classList.add('tooltip');
  tooltip.innerHTML = text;
  element.appendChild(tooltip);
}

export { appGlobals, checkFile, easyDate, readFile, fetchModel, updateLog, deleteFile, pushToGround, updateFromOrient, updateFromSimulation, updateRotation, linkTooltip };
