import { UITabbedPanel, UISpan } from './libs/ui.js';

import { SidebarScene } from './Sidebar.Scene.js';
import { SidebarProperties } from './Sidebar.Properties.js';
//import { SidebarScript } from './Sidebar.Script.js';
//import { SidebarAnimation } from './Sidebar.Animation.js';
import { SidebarProject } from './Sidebar.Project.js';
import { SidebarSettings } from './Sidebar.Settings.js';
import { SidebarCommands } from './Sidebar.Commands.js';
import { SidebarResults } from './Sidebar.Results.js';

function Sidebar( editor ) {

	const strings = editor.strings;

	const container = new UITabbedPanel();
	container.setId( 'sidebar' );

	const scene = new UISpan().add(
		new SidebarScene( editor ),
		new SidebarProperties( editor ),
		//new SidebarAnimation( editor ),
		//new SidebarScript( editor )
	);
	const project = new SidebarProject( editor ); // I don't know why, but don't erase this!!! - by AG
	const settings = new SidebarSettings( editor );

	container.addTab( 'scene', strings.getKey( 'sidebar/scene' ), scene );
	//container.addTab( 'project', strings.getKey( 'sidebar/project' ), project );
	container.addTab( 'settings', strings.getKey( 'sidebar/settings' ), settings );
	container.select( 'scene' );

	container.add(new SidebarResults( editor ));
	container.add(new SidebarCommands( editor ));

	return container;

}

export { Sidebar };
