<h1>ProMED log:</h1>
<?php
$file = '/home/volumes/log/log.json';
if (file_exists($file)) {    
  $log = json_encode(json_decode(file_get_contents($file)), JSON_PRETTY_PRINT);
  echo '<pre>' . $log . '</pre>';
} else {
  echo '<p>Log file not found</p>';
}

