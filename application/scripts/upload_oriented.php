<?php
  
  function easyDate() {
  	$now = date_create('now');
  	return $now->format('Ymd') . 'T' . $now->format('His');
  }

  // Checking file existence - change in production
  if ($_FILES["model"]["error"] != UPLOAD_ERR_OK && $_FILES["model"]["error"] != UPLOAD_ERR_NO_FILE) {
    print_r($_FILES["model"]["error"]);
    exit(" - Error in the 3D model file uploaded");
  }

  $model_destination = isset($_POST["filename"]) ? $_POST["filename"] : easyDate . $_FILES['model']['name'];

  // Checking folder existence
  $destination = '/home/volumes/upload/models/oriented/';
  if (!file_exists($destination)) {
    mkdir($destination, 0777);    
  }

  // Adding file to destination folder
  $model_destination = $destination . $model_destination;
  $model_file = $_FILES["model"]["tmp_name"];
  move_uploaded_file($model_file, $model_destination);  
  exit($model_destination);