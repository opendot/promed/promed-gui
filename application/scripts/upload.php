<?php
  function easyDate() {
    $now = date_create('now');
    return $now->format('Ymd') . 'T' . $now->format('His');
  }
  
  $return_object = new stdClass();

  // Checking file existence - change in production - by AG
  if ($_FILES["model"]["error"] != UPLOAD_ERR_OK && $_FILES["model"]["error"] != UPLOAD_ERR_NO_FILE) {
      print_r($_FILES["model"]["error"]);
    exit(" - Error in the 3D model file uploaded");
  }
  if (!isset($_POST["orient"])) {
    exit("Orientation data not set");
  }
  if (!isset($_POST["simulation"])) {
    exit("Simulation data not set");
  }

  // Check if log file exist, otherwise creates it
  $log = '/home/volumes/log/log.json';
  if (!file_exists($log)) {
    touch($log);
    $emptyLog = array (
      "models" => array(),
      "parameters" => array(),
      "operations" => array(
        "orients" => array(),
        "simulations" => array()
      )
    );
    file_put_contents($log, json_encode($emptyLog));
    // echo nl2br("Log file not found. Created new log.json file" . PHP_EOL);
  }
  $tmp = json_decode(file_get_contents($log));

  // file names will be encoded as ISO 8601 date of current moment
  $model_date = easyDate();
  $model_name = $_POST['modelName'];
  // Check if filename already exists and in case add timestamp
  if ($_POST['existingModel'] == 'false') {
    $flag = false;
    foreach($tmp->models as $key => $value) {
      if ($value->name === $model_name) { $flag = true; }
    }
    if ($flag) { $model_name = $model_date . '_' . $model_name; }
  }
  $model_destination = '/home/volumes/upload/models/' . $model_name;
  // Check if it's an already existing file. If not, copy files to upload folder
  if ($_POST['existingModel'] == 'false') {
    $model_file = $_FILES["model"]["tmp_name"];
    move_uploaded_file($model_file, $model_destination);
  }

  // Set parameters file name
  $parameters_date = easyDate();
  $parameters_name = $parameters_date . '.json';
  $parameters_destination = '/home/volumes/upload/parameters/' . $parameters_name;
  
  // Convert jsons to files and put in upload folder
  $orient_settings = $_POST["orient"];
  $simulation_settings = $_POST["simulation"];
  $input_settings = $_POST['inputFields'];

  $tmp_parameters = new stdClass();
  $tmp_parameters->orient = json_decode($orient_settings);
  $tmp_parameters->simulation = json_decode($simulation_settings);
  $tmp_parameters->input_fields = json_decode($input_settings);

  touch($parameters_destination);
  file_put_contents($parameters_destination, json_encode($tmp_parameters));

  // Setup new log entry
  $model_entry = array (
    'timestamp' => easyDate(),
    'name' => $model_name
  );

  $parameters_entry = array (
    'timestamp' => $parameters_date,
    'name' => $parameters_name,
    'model' => $model_name
  );

  // Add new entry into log 
  if ($_POST['existingModel'] == 'false') {
    $tmp->models []= $model_entry;
  }  
  $tmp->parameters []= $parameters_entry;
  file_put_contents($log, json_encode($tmp));

  $return_object->output = nl2br("Log updated" . PHP_EOL);

  // TO DO - Sync with bucket - by AG
  # $output = shell_exec("/home/scripts/syncing.sh 2>&1");
  # $return_object->output .= nl2br($output . PHP_EOL);
  # $return_object->output .= nl2br("Upload of model in remote completed:" . PHP_EOL);

  // Copy files in folders for processing
  // TO DO - Think about a better strategy to not leave behind a breadcrumb trail of tmp files - by AG
  $orient_file = './orient.json';
  touch($orient_file);
  file_put_contents($orient_file, json_encode($tmp_parameters->orient));

  $simulation_file = './simulation.json';
  touch($simulation_file);
  file_put_contents($simulation_file, json_encode($tmp_parameters->simulation));

  $output = shell_exec("/home/scripts/preprocessing.sh ". $model_destination . " " . $orient_file . " " . $simulation_file . " 2>&1");
  $return_object->output .= nl2br($output . PHP_EOL);

  // Model orientation & simulation
  $return_object->output .= nl2br("Starting processing..." . PHP_EOL);
  $output = shell_exec("/home/scripts/processing.sh 2>&1");
  $return_object->output .= nl2br($output . PHP_EOL);
  $return_object->output .= nl2br("Processing completed" . PHP_EOL );

  // Rename and move results to output volume
  $result_date = easyDate();
  $orient_name = $result_date . '.json';
  $simulation_name = $result_date . '.json';
  $orient_destination = '/home/volumes/orient-output/' . $orient_name;
  $simulation_destination = '/home/volumes/simulation-output/' . $simulation_name;
  copy('/home/volumes/shared/results/orient/output.json', $orient_destination);
  copy('/home/volumes/shared/results/simulation/output.json', $simulation_destination);

  // Add new entries into log
  $orient_entry = array (
    'timestamp' => $result_date,
    'name' => $orient_name,
    'model' => $model_name,
    'parameters' => $parameters_name
  );
  $simulation_entry = array (
    'timestamp' => $result_date,
    'name' => $simulation_name,
    'model' => $model_name,
    'parameters' => $parameters_name
  );
  $tmp = json_decode(file_get_contents($log));
  $tmp->operations->orients []= $orient_entry;
  $tmp->operations->simulations []= $simulation_entry;
  file_put_contents($log, json_encode($tmp));
  $return_object->output .= nl2br(PHP_EOL . "Log updated" . PHP_EOL);

  // TO DO - Sync with cloud - by AG

  // Print results
//  $file = '/home/volumes/shared/results/orient/output.json';
//  if (file_exists($file)) {
//    echo nl2br("Model orientation:" . PHP_EOL);
//    $log = json_encode(json_decode(file_get_contents($file)), JSON_PRETTY_PRINT);
//    echo '<pre>' . $log . '</pre>';
//  }
//
//  $file = '/home/volumes/shared/results/simulation/output.json';
//  if (file_exists($file)) {
//    echo nl2br("Model simulation:" . PHP_EOL);
//    $log = json_encode(json_decode(file_get_contents($file)), JSON_PRETTY_PRINT);
//    echo '<pre>' . $log . '</pre>';
//  }

$return_object->model_destination = $model_destination;
$return_object->parameters_destination = $parameters_destination;
$return_object->orient_destination = $orient_destination;
$return_object->simulation_destination = $simulation_destination;

exit(json_encode($return_object));
