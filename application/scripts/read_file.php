<?php
$url = $_POST['url'];
$extension = preg_split('/\./', $url);
$extension = $extension[sizeof($extension) - 1];
$filename = preg_split("/.*\//", $url);
$filename = $filename[1];
switch ($extension) {
  case 'json':
    if (file_exists($url)) {
      readfile($url);
    } else {
      exit ('Error in reading file');
    }
    break;
  case 'stl':
    if (file_exists($url)) {
      touch('./' . $filename);
      copy($url, $filename);
      echo 'scripts/' . $filename;
    } else {
      exit ('Error in reading file');
    }
    break;
  default:
    break;
  exit();
}