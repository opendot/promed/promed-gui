FROM nginx:alpine AS https-server

# Comment out for development stage - by AG
COPY application/ /tmp/

COPY default.conf /etc/nginx/conf.d

# Nano is for debug purpose - by AG
RUN apk add nano